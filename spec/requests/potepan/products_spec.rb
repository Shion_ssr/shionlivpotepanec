require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /potepan/products/show" do
    let(:product) { create(:product) }

    before { get spree.product_path(product) }

    it "レスポンスが正しく返ってくるか" do
      expect(response).to have_http_status 200
    end

    it "商品ページが表示されるか" do
      expect(response).to render_template(:show)
    end

    it "商品情報が正しく表示されているか" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end
  end
end
