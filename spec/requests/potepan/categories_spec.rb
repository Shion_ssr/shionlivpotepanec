require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET potepan/categories/show" do
    let(:taxon) { create(:taxon) }
    let(:taxonomy) { create(:taxonomy) }
    let(:product) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "レスポンスが正しく返ってくるか" do
      expect(response).to have_http_status 200
    end

    it "showページに正しくtaxonomyが取得できているか" do
      expect(response.body).to include taxonomy.name
    end

    it "showページに正しくtaxonが取得できているか" do
      expect(response.body).to include taxon.name
    end

    it "taxonに紐づく商品情報が正しく取得されているか" do
      taxon.products.each do |product|
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price.to_s
        expect(response.body).to include product.images.attachment.call(:product)
      end
    end
  end
end
