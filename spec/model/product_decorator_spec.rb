require 'rails_helper'

RSpec.describe 'Potepan::ProductDecorator', type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:not_related_products) { create(:product) }

  describe "related_products" do
    it "related_productメソッドが正しく関連商品を取得しているか" do
      expect(product.related_products).to eq related_products
    end

    it "関連しない商品が含まれていないこと" do
      expect(product.related_products).not_to include not_related_products
    end

    it "商品詳細ページの関連商品に、開いてる詳細商品自身が含まれないこと" do
      expect(product.related_products).not_to include product
    end

    it "関連商品の個数が正しいか" do
      expect(product.related_products.count).to eq related_products.count
    end

    it "同じ関連商品が表示されていないか" do
      expect(product.related_products.uniq).to eq product.related_products
    end
  end
end
