require "rails_helper"

RSpec.describe "Product", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:not_related_products) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "関連商品を正しく表示" do
    within('.productsContent') do
      expect(page).to have_content related_products.first.name
      expect(page).to have_content related_products.first.display_price
    end
  end

  scenario "関連商品を押したとき正しく商品詳細ページを表示" do
    click_on related_products.first.name
    expect(current_path).to eq potepan_product_path(related_products.first.id)
  end

  context "関連商品の表示数" do
    scenario "関連商品を4件表示する" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end

  scenario "関連しない商品が表示されないテスト" do
    within('.productsContent') do
      expect(page).not_to have_content not_related_products
    end
  end
end
