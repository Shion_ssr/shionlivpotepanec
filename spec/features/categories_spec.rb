require 'rails_helper'

RSpec.feature 'Potepan::Categories', type: :feature do
  let(:taxon) { create(:taxon, parent: taxonomy.root) }
  let(:taxonomy) { create(:taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:image) { build(:image) }

  before do
    product.images << image
    visit potepan_category_path(taxon.id)
  end

  describe '商品カテゴリーバー' do
    scenario "正しいtaxonomyとtaxonが表示されていること" do
      within(:css, '.navbar-side-collapse') do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content "#{taxon.name} (#{taxon.all_products.count})"
      end
    end

    scenario "分類商品を押した時正しいカテゴリーページに移動" do
      within(:css, ".navbar-side-collapse") do
        click_link taxon.name, match: :first
      end
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  describe '商品を押した時' do
    scenario "正しく商品詳細ページに移動 一覧に戻る" do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end
  end
end
